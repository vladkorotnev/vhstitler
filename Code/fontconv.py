#!/bin/env python
from PIL import Image
im = Image.open('sg8bit_font_8x8.png', 'r')
pix = im.load()

char_names = [
	["idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk"],
	["idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk"],
	["idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk"],
	["idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk"],
	["idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk"],
	["idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk","idk"],
	["idk","idk","idk","idk","idk","idk","wo","xa","xi","xu","xe","xo","xya","xyu","xyo","xtu"],
	["~","a","i","u","e","o","ka","ki","ku","ke","ko","sa","si","su","se","so"],
	[" ",".","q1","q2",",","dot","wo","xa","xi","xu","xe","xo","xya","xyu","xyo","xtu"],
	["-","a","i","u","e","o","ka","ki","ku","ke","ko","sa","si","su","se","so"],
	["ta","ti","tu","te","to","na","ni","nu","ne","no","ha","hi","fu","he","ho","ma"],
	["mi","mu","me","mo","ya","yu","yo","ra","ri","ru","re","ro","wa","n","dakuon","handakuon"],
	["ta","ti","tu","te","to","na","ni","nu","ne","no","ha","hi","fu","he","ho","ma"],
	["mi","mu","me","mo","ya","yu","yo","ra","ri","ru","re","ro","wa","n","dakuon","handakuon"]
]

def extract_char_at(startx, starty):
	global pix
	cur_char = ""
	for y in range(starty, starty+8):
		cur_line = "0b"
		for x in range(startx, startx+8):
			if pix[x,y] > 0:
				cur_line += "1"
			else:
				cur_line += "0"
		cur_line += ","
		cur_char += cur_line
		cur_char += "\n"
	return cur_char

for l in range(0, 14):
	print "// Line ",l
	print "\n"
	for c in range(0,16):
		print "// Char ",c,char_names[l][c]
		print extract_char_at(c*8, l*8)