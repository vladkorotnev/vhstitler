#include <PS2uartKeyboard.h>
#include <TVout.h>
#include <fontALL.h>
#include <PS2uartKeyboard.h>

#include <TVout.h>
#include <fontALL.h>
#include "genjitsu.h"
#include "hikarin_font.h"

#define DISPLAY_W 120
#define DISPLAY_H 96

TVout tv;
PS2uartKeyboard keyboard;



// -----------------------------------------
//         K E Y B O A R D
// -----------------------------------------
char waitchar() {
 while(!haschar()) {/* wait for char */}
 return getch(); 
}

void wait_specific_char(char waitfor) {
  while(true) {
    if(haschar()) {
       if(getch() == waitfor) { key_sound(); return; }
       error_sound();
    }
  }
}

boolean haschar() {
  return keyboard.available();
}

char getch() {
  if(!haschar()) return 0;
  return (char)keyboard.read(); 
}


// -----------------------------------------
//            S O U N D 
// -----------------------------------------
void success_sound() {
  tv.tone(1000,50);
}
void key_sound() {
 tv.tone(1500,20); 
}
void error_sound() {
  tv.tone(500,100);
}
void prompt_sound() {
  tv.tone(1000,50);
  tv.tone(2500,50);
}


// -----------------------------------------
//             T V   O U T P U T
// -----------------------------------------

void start_output() {
    tv.begin(PAL, DISPLAY_W, DISPLAY_H); 
  // Disable output and PWM on pins 9 and 10 (thus removing the synchro provided by TVOUT)
  TCCR1A  =  0;
   // Enable timer1.  ICES0 is set to 0 for falling edge detection on input capture pin.
 TCCR1B = _BV(CS10);
  // Enable input capture interrupt
  TIMSK1 |= _BV(ICIE1);
  // Enable external interrupt INT0 on pin 2 with falling edge.
  EIMSK = _BV(INT0);
  EICRA = _BV(ISC01);
     tv.set_hbi_hook(keyboard.begin());
    do_intro();
}

// Required to reset the scan line when the vertical sync occurs
ISR(INT0_vect) {
  display.scanLine = 0;
}


void do_intro() {
  tv.select_font(fontxnu);
  tv.bitmap(0,10,genjitsu_logo);
  tv.print(0,83,"  Gadget 003");
  success_sound();
  tv.print(0,83," Version 1.3");
  tv.delay(500); 
  tv.fill(0);
}

void clrscr() {
  tv.fill(0); 
}


// -----------------------------------------
//          C u b e   E f f e c t
// -----------------------------------------

int zOff = 150;
int xOff = 0;
int yOff = 0;
int cSize = 50;
int view_plane = 64;
float angle = PI/60;
boolean cube_clears = true;
unsigned int cube_counter = 0;
unsigned int cube_clr_divisor = 0;
float cube3d[8][3] = {
  {xOff - cSize,yOff + cSize,zOff - cSize},
  {xOff + cSize,yOff + cSize,zOff - cSize},
  {xOff - cSize,yOff - cSize,zOff - cSize},
  {xOff + cSize,yOff - cSize,zOff - cSize},
  {xOff - cSize,yOff + cSize,zOff + cSize},
  {xOff + cSize,yOff + cSize,zOff + cSize},
  {xOff - cSize,yOff - cSize,zOff + cSize},
  {xOff + cSize,yOff - cSize,zOff + cSize}
};
unsigned char cube2d[8][2];

void cube_run() {
  while(true) {
    if(haschar()) {
       char keystroke = getch();
       
      switch(keystroke) {
          
  
        case PS2_PAGEUP:
          if(view_plane < 255) 
            view_plane += 1;
          break;
        case PS2_PAGEDOWN:
          if(view_plane > 0) 
            view_plane -= 1;
          break;
          
        case PS2_UPARROW:
           yOff += 1;
          break;
        case PS2_DOWNARROW:
           yOff -= 1;
          break;  
          
        case PS2_LEFTARROW:
          xOff -= 1;
          break;
        case PS2_RIGHTARROW:
          xOff += 1;
          break;
        
        
  
        case PS2_F1:
            zOff -= 1;
          break;
        case PS2_F2:
            zOff += 1;
          break;
        case PS2_F3:
            cube_clears = !cube_clears;
          break;
        case PS2_F4:
  
          break;
          
       case PS2_F5:
         if(cube_clr_divisor > 0) 
           --cube_clr_divisor;
         break;
         
       case PS2_F6:
         if(cube_clr_divisor < 255)
           ++cube_clr_divisor;
         break;
      
        // DEL, Enter, ESC -- stop cube
        case PS2_DELETE:
        case PS2_ENTER:
        case PS2_ESC:
          prompt_sound();
          return;
          break;
       
        default:
          break;
      } 
    } else 
      cube_step();
  }//end while
}

void cube_step() {
  int rsteps = random(10,60);
  switch(random(6)) {
    case 0:
      for (int i = 0; i < rsteps; i++) {
        zrotate(angle);
        printcube();
      }
      break;
    case 1:
      for (int i = 0; i < rsteps; i++) {
        zrotate(2*PI - angle);
        printcube();
      }
      break;
    case 2:
      for (int i = 0; i < rsteps; i++) {
        xrotate(angle);
        printcube();
      }
      break;
    case 3:
      for (int i = 0; i < rsteps; i++) {
        xrotate(2*PI - angle);
        printcube();
      }
      break;
    case 4:
      for (int i = 0; i < rsteps; i++) {
        yrotate(angle);
        printcube();
      }
      break;
    case 5:
      for (int i = 0; i < rsteps; i++) {
        yrotate(2*PI - angle);
        printcube();
      }
      break;
  }
}


void printcube() {
  //calculate 2d points
  for(byte i = 0; i < 8; i++) {
    cube2d[i][0] = (unsigned char)((cube3d[i][0] * view_plane / cube3d[i][2]) + (tv.hres()/2));
    cube2d[i][1] = (unsigned char)((cube3d[i][1] * view_plane / cube3d[i][2]) + (tv.vres()/2));
  }
  tv.delay_frame(1);
  if(cube_clears) {
   if(cube_counter >= cube_clr_divisor) {
    tv.fill(0); 
    cube_counter = 0; 
   } else cube_counter++;
  }
  draw_cube();
}

void zrotate(float q) {
  float tx,ty,temp;
  for(byte i = 0; i < 8; i++) {
    tx = cube3d[i][0] - xOff;
    ty = cube3d[i][1] - yOff;
    temp = tx * cos(q) - ty * sin(q);
    ty = tx * sin(q) + ty * cos(q);
    tx = temp;
    cube3d[i][0] = tx + xOff;
    cube3d[i][1] = ty + yOff;
  }
}

void yrotate(float q) {
  float tx,tz,temp;
  for(byte i = 0; i < 8; i++) {
    tx = cube3d[i][0] - xOff;
    tz = cube3d[i][2] - zOff;
    temp = tz * cos(q) - tx * sin(q);
    tx = tz * sin(q) + tx * cos(q);
    tz = temp;
    cube3d[i][0] = tx + xOff;
    cube3d[i][2] = tz + zOff;
  }
}

void xrotate(float q) {
  float ty,tz,temp;
  for(byte i = 0; i < 8; i++) {
    ty = cube3d[i][1] - yOff;
    tz = cube3d[i][2] - zOff;
    temp = ty * cos(q) - tz * sin(q);
    tz = ty * sin(q) + tz * cos(q);
    ty = temp;
    cube3d[i][1] = ty + yOff;
    cube3d[i][2] = tz + zOff;
  }
}

void draw_cube() {
  tv.draw_line(cube2d[0][0],cube2d[0][1],cube2d[1][0],cube2d[1][1],WHITE);
  tv.draw_line(cube2d[0][0],cube2d[0][1],cube2d[2][0],cube2d[2][1],WHITE);
  tv.draw_line(cube2d[0][0],cube2d[0][1],cube2d[4][0],cube2d[4][1],WHITE);
  tv.draw_line(cube2d[1][0],cube2d[1][1],cube2d[5][0],cube2d[5][1],WHITE);
  tv.draw_line(cube2d[1][0],cube2d[1][1],cube2d[3][0],cube2d[3][1],WHITE);
  tv.draw_line(cube2d[2][0],cube2d[2][1],cube2d[6][0],cube2d[6][1],WHITE);
  tv.draw_line(cube2d[2][0],cube2d[2][1],cube2d[3][0],cube2d[3][1],WHITE);
  tv.draw_line(cube2d[4][0],cube2d[4][1],cube2d[6][0],cube2d[6][1],WHITE);
  tv.draw_line(cube2d[4][0],cube2d[4][1],cube2d[5][0],cube2d[5][1],WHITE);
  tv.draw_line(cube2d[7][0],cube2d[7][1],cube2d[6][0],cube2d[6][1],WHITE);
  tv.draw_line(cube2d[7][0],cube2d[7][1],cube2d[3][0],cube2d[3][1],WHITE);
  tv.draw_line(cube2d[7][0],cube2d[7][1],cube2d[5][0],cube2d[5][1],WHITE);
}

// -----------------------------------------
//       H  i  k  a  r  i  n ENGINE
// -----------------------------------------

// Whether to use hiragana or katakana font
boolean hikarin_hiragana = false;

// The char code to be output (in Hikarin font)
unsigned char hikarin_output = 0;

// The char code to be post-output
unsigned char hikarin_post_out = 0;

void hikarin_write(char outchar) {
  tv.write_force(outchar); 
}

void hikarin_backspace() {
 tv.write(PS2_BACKSPACE); 
}

void hikarin_flush() {
  tv.select_font(hikarin_hiragana ? hikarin_hira : hikarin_kata);
   hikarin_write(hikarin_output);
   hikarin_output = 0;
   if(hikarin_post_out > 0) {
     hikarin_write(hikarin_post_out);
     hikarin_post_out = 0; 
   }
   tv.select_font(font8x8);
   key_sound();
}

void hikarin_clear() {
  if(hikarin_output > 0) {
   hikarin_backspace();
  } 
  hikarin_output = 0;
  hikarin_post_out = 0;
}

const String hikarin_first_row = "aiueo";
const String hikarin_prefix_row = "kstnhwymrx";
const String hikarin_dakuon_row = "gzjdb";
const char hikarin_daku_map[] = {0,1,1,2,4};
                    // from:     g z j d b
                    //   to:     k s s t h
                          
void hikarin_input(char inchar) {
  int symidx;
  if((symidx = hikarin_first_row.indexOf(inchar)) > -1) {
    // we are in main row 
    // if we have a prefix char on screen, erase it
    if(hikarin_output > 0) hikarin_backspace();
    // add hikarin font offset
    hikarin_output += symidx;
    hikarin_flush();
  } else if (((symidx = hikarin_prefix_row.indexOf(inchar)) > -1)
            && hikarin_output == 0) {
    // we are in prefix row and this is first char
    // offset the hikarin font pointer to point at the row with
    // this prefix
    hikarin_output += (symidx+1) * 5;
    hikarin_write(inchar);
  } else if (inchar == 'n' && hikarin_output > 0) {
    // we have N as second char, output N kana 
    hikarin_backspace();
    hikarin_output = 31; // N kana
    hikarin_flush();
  } else if (((symidx = hikarin_dakuon_row.indexOf(inchar)) > -1) 
             && hikarin_output == 0) {
    // we have dakuon input, remap and act as if prefix row 
    hikarin_write(inchar);
    hikarin_output += (hikarin_daku_map[symidx]+1)*5;
    hikarin_post_out = 36;
  } else if (inchar == '.' && hikarin_output == 0) {
    // output dot 
    hikarin_output = 32;
    hikarin_flush();
  } else if (inchar == ','  && hikarin_output == 0) {
    // output comma 
    hikarin_output = 33;
    hikarin_flush();
  } else if (inchar == 'p'  && hikarin_output == 0) {
    // set post-out to handakuon 
    hikarin_write(inchar);
    hikarin_output += 25;
    hikarin_post_out = 38; 
  } else if (inchar == '-' && hikarin_output == 0){
    hikarin_output = 55;
    hikarin_flush();
  } else error_sound();
}


// -----------------------------------------
void setup()  { 
 start_output();
}

boolean delete_phase = false;

#define INPUT_RAWK 0
#define INPUT_ASCC 1
#define INPUT_KANA 2
unsigned char current_mode = INPUT_RAWK;


char current_code = 0;

void loop() {
   if(haschar()) {
     
     char keystroke = getch();
     if(keystroke != PS2_DELETE) delete_phase = false;
     
    switch(keystroke) {
      case PS2_TAB:
        if(current_mode == INPUT_KANA) {
          if(hikarin_hiragana == true)
          {
            hikarin_hiragana = false;
            prompt_sound();
          } else {
            hikarin_hiragana = true; 
            success_sound();
          }
          
        }
        break;
      
      case PS2_ESC:
        if(current_mode == INPUT_ASCC) {
           if(current_code == 0) {
              key_sound(); current_mode = INPUT_RAWK;
           } else {
              prompt_sound(); current_code = 0; 
           }
        }
        break;
      case PS2_INSERT:
        
        break;
        
      case PS2_HOME:
        success_sound();
        for(unsigned char i = 0; i <= DISPLAY_H; i++) {
          tv.shift(2, UP);
          tv.delay_frame(1);
        }
        break;
      
       case PS2_END:
        success_sound();
        for(unsigned char i = 0; i <= DISPLAY_H; i++) {
          tv.shift(2, DOWN);
          tv.delay_frame(1);
        }
         break;
       
      case PS2_PAGEUP:
       key_sound();
         tv.shift(10, UP);
        break;
      case PS2_PAGEDOWN:
        key_sound();
         tv.shift(10, DOWN);
        break;
      case PS2_UPARROW:
         key_sound();
         tv.shift(1, UP);
        break;
      case PS2_LEFTARROW:
        key_sound();
         tv.shift(1, LEFT);
        break;
      case PS2_RIGHTARROW:
        key_sound();
         tv.shift(1, RIGHT);
        break;
      case PS2_DOWNARROW:
        key_sound();
         tv.shift(1, DOWN);
        break;
      case PS2_SCROLL:
        break;
      
       // f1-f4 : english fonts
      case PS2_F1:
        if(current_mode != INPUT_KANA) {
          tv.select_font(font4x6);
          success_sound();
        }
        break;
      case PS2_F2:
        if(current_mode != INPUT_KANA) {
          tv.select_font(font6x8);
          success_sound();
        }
        break;
      case PS2_F3:
       if(current_mode != INPUT_KANA) {
          tv.select_font(font8x8);
          success_sound();
        }
        break;
      case PS2_F4:
         if(current_mode != INPUT_KANA) {
          tv.select_font(fontxnu);
          success_sound();
        }
        break;
        
        // f5 : russian font
        // f6 : katakana font
        // f7 : 
        // f8 : input ascii codes
      case PS2_F5:
        if(current_mode != INPUT_KANA) {
          tv.select_font(font6x8ru);
          success_sound();
        }
        break;
        
      case PS2_F6:
        if(current_mode != INPUT_KANA) {
           prompt_sound();
           current_mode = INPUT_KANA; 
           hikarin_clear();
            tv.select_font(font8x8);
        } else {
           success_sound();
           current_mode = INPUT_RAWK; 
        }
        break;
      case PS2_F7:
      
        break;
        
      case PS2_F8:
        if(current_mode != INPUT_ASCC) {
           prompt_sound();
           current_mode = INPUT_ASCC; 
           current_code = 0;
           hikarin_clear();
        } else {
           success_sound();
           current_mode = INPUT_RAWK; 
        }
        break;

       // f11 : cube 
       // f12 : logo
      case PS2_F9:
        
        break;
      case PS2_F10:
        
        break;
      case PS2_F11:
         success_sound();
         cube_run();
         success_sound();
        break;
      case PS2_F12:
         tv.bitmap(0,10,genjitsu_logo);
         success_sound();
        break;
        
      // 2x delete -- clr scr
      case PS2_DELETE:
        if(!delete_phase) {
           prompt_sound();
          delete_phase = true; 
        }
        else {
          tv.fill(0);
          delete_phase = false;
          success_sound();
          hikarin_clear();
          
        }
        break;
      case PS2_ENTER:
          if(current_mode == INPUT_RAWK || current_mode == INPUT_KANA) {
             tv.println("");
            key_sound(); 
            hikarin_clear();
          } else if (current_mode == INPUT_ASCC) {
            tv.print(current_code);
            prompt_sound();
          }
        break;
     
      default:
         if(current_mode == INPUT_RAWK) {
           key_sound();
           tv.print(keystroke);
         } else if (current_mode == INPUT_ASCC) {
           if(keystroke >= '0' && keystroke <= '9') {
             key_sound();
             current_code = current_code * 10 + (keystroke - '0');
             if(current_code > 255) {
                error_sound(); current_code = 0; 
             }
           } else error_sound();
         } else if (current_mode == INPUT_KANA) {
           hikarin_input(keystroke);
         }
        break;
    } 
   }
}

