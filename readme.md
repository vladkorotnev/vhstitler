# Genjitsu Gadget 003 — VHS Title Generator
#### Version 1.3

Visit Genjitsu Gadget Lab at http://genjit.su!

## Description

Plugs into your camera and generates a text overlay with anything you type in, supports Russian and Japanese (Katakana) as well.

## Parts

What I used to assemble this:

* LM1881 sync separator
* some perfboard
* Arduino nano (clone version from China)
* Logitech black internet PS/2 keyboard
* PS/2 connector to replace my Panasonic M3500's character generator port

## Electronics

This directory contains the details on my implementation.
Note that the layout for the perfboard is displayed as the top view (basically what is covered by the Arduino Nano when it's plugged in) and that from the said top view the LM1881 is upside down. 

It also contains the pin mapping that I used to replace the character generator port with a PS/2 connector on my Panasonic M3500 in order to use a standard PS/2 keyboard's 4-wire cord. 

Initially I planned to install it inside the camera and be able to use any PS/2 keyboard but flashing the firmware required taking apart the camera that way and that would be way harder than taking apart a keyboard :)

## Code

This directory contains the PS2UART library to read the keyboard with all the mappings restored to ASCII standards (almost — but at least it all works), a modded TVOut library to allow printing the unprintable characters, and 2 extra TVOutFonts (Russian 6x8 pixel and Apple XNU kernel font). Put the libraries into your Arduino libraries folder. 
The sketch itself is contained inside the Titler_Simple folder, along with two resource files, one with the Genjitsu logo and some icons, and the other with a Japanese Katakana 8x8 font for Genjitsu HiKaRIn engine. 

## Key map

* F1: 4x6 English Font
* F2: 6x8 English Font
* F3: 8x8 English Font
* F4: Apple Darwin/XNU Font (8x16 size)
* F5: Russian 6x8 Font
* F6: Toggle HiKaRIn (Japanese input)
* F8: Toggle ASCII code input (input number of symbol, Enter inputs the symbol, ESC resets the number, 2x ESC turns the mode off)
* F12: Display Genjitsu Logo
* 2x DEL: Clear Screen
* Arrows: Move Screen
* PgUp/PgDn: Move Screen 10 lines up/down
* Home/End: Animate scroll up/down (1 line per frame)